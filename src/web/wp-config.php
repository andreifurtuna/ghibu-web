<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'c11dbase');

/** MySQL database username */
define('DB_USER', 'c11wpuser');

/** MySQL database password */
define('DB_PASSWORD', 'JnCTaKDnky3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S#!1 ;8!1NPkzxi_3(BIeq-nE:~Gb.PO02`4jwlFV#<W<k2!=g9{D(tp>Bo#o4KY');
define('SECURE_AUTH_KEY',  '{dr QZo#LEB32-Of#johzZ]/|YAFCTL$Lml[j}Bnf4PqCoz=Q##uKZGtF[pCMq<}');
define('LOGGED_IN_KEY',    '}rp>;/U9tS<*<0{~?i=Z(J*iDR7rT`N(yz!:o3sZLs;iPhGi,JPQH4.Qkt^ItU4M');
define('NONCE_KEY',        'QKBh4]SiUs_9,F@6SBq,NkvkaLqvUk][`KW|M-xD[Up9lem4gyX42Z_}:3Dp;q@t');
define('AUTH_SALT',        'b?bGWCIRl*-Rh3m~QXBRsImGss;|W:VJuo*VXb)J,2n8mCt{QopG6LP{3g-nD/no');
define('SECURE_AUTH_SALT', 'i|cfZo,(+Pz`{OI_ ,1}_I+?^aaM5xHX)P?2-qN1$yQ^cSL!wysL//_qP-|X=M@K');
define('LOGGED_IN_SALT',   'CIsEp;//X1K6Y78>XSQAQLz->wH-di1U=Mf$s8MR`{%;!m@&qGNW`E>D_pBvLaO}');
define('NONCE_SALT',       '3.9@D.6rUi6&lR}PI2vP+CYfIlMy~^DU*db`~_-C,R%3&K5)-lU$(uSvZvrU]@.&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
